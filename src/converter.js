/**
 * @author Aleksi - padding outputs 2 characters always
 * @param {String} hex - one or two characters
 * @returns {String} hex with two characters
 */
const pad = (hex) => hex.length === 1 ? '0' + hex : hex;

module.exports = {
    /**
    * @author Aleksi - converts RGB value to Hexcode
    * @param {Number} red - 0-255
    * @param {Number} green - 0-255
    * @param {Number} blue - 0-255
    * @returns {String} color as a hexcode
    */
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16); // 0-255 -> 0ff
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);
        const hex = pad(redHex) + pad(greenHex) + pad(blueHex);
        return hex;
    },
    /**
    * @author Aleksi - converts Hex to RGB
    * @param {String} hex - hexcode (6 chars)
    * @returns {Object} RGB values
    */
    hexToRgb: (hex) => {
        if(hex.length !== 6) return "Invalid hexcode";
        const red = parseInt(hex[0] + hex[1], 16);
        const green = parseInt(hex[2] + hex[3], 16);
        const blue = parseInt(hex[4] + hex[5], 16);
        return {red,green,blue};
    }
}

