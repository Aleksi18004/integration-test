// Integration test
const expect = require('chai').expect;
const request = require('request');
const app = require('../src/server');
const port = 3000;

describe('Color Code Converter API', () => {
    before('Start server before run tests', (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening: localhost:${port}`);
            done();
        })
    });

    describe('RGB to Hex conversion', () => {
        const urlRgbToHex = `http://localhost:${port}/rgb-to-hex?red=255&green=255&blue=255`;
        const urlHexToRgb = `http://localhost:${port}/hex-to-rgb?hex=ffffff`;

        it('returns status 200', (done) => {
            request(urlRgbToHex, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });

        it('returns status 200', (done) => {
            request(urlHexToRgb, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });

        it('returns the color in hex', (done) => {
            request(urlRgbToHex, (error, response, body) => {
                expect(JSON.parse(body).data).to.equal('ffffff');
                done();
            });
        });

        it('returns the color in rgb', (done) => {
            request(urlHexToRgb, (error, response, body) => {
                body = JSON.parse(body).data;
                expect(body.red).to.equal(255);
                expect(body.green).to.equal(255);
                expect(body.blue).to.equal(255);
                done();
            });
        });
    });

    after('Stop server after tests', (done) => {
        server.close();
        done();
    });
    
})
