const bruh = document.getElementById('audio-bruh');
const wow = document.getElementById('audio-wow');

for (let button of document.getElementsByClassName('convert-submit')) {
    button.addEventListener('click', (e) => {
        e.preventDefault();
    });
}
const convert = (type) => {
    switch (type) {
        case 'hex':
            let hex = document.getElementById('hexcode-input').value;
            if (hex.length !== 6) {
                bruh.play();
                return;
            }

            new Promise(async (res, rej) => {
                try {
                    let response = await fetch(`http://localhost:3000/hex-to-rgb?hex=${hex}`);
                    let data = await response.json();
                    let red = parseInt(data.data.red);
                    let green = parseInt(data.data.green);
                    let blue = parseInt(data.data.blue);
                    if(isNaN(red) || isNaN(green) || isNaN(blue)){
                        bruh.play();
                        return;
                    }
                    let rgb = `rgb(${red}, ${green}, ${blue})`;
                    document.getElementById('rgb-color-container').style.backgroundColor = rgb;
                    document.getElementById('rgb-output').innerHTML = rgb;
                    wow.play();
                } catch (error) {
                    bruh.play();
                }
                
            });
            break;
        case 'rgb':
            let red = parseInt(document.getElementById('red-input').value);
            let green = parseInt(document.getElementById('green-input').value);
            let blue = parseInt(document.getElementById('blue-input').value);
            if (isNaN(red) || isNaN(green)|| isNaN(blue) || red < 0 || red > 255 ||  green < 0 || green > 255 ||  blue < 0 || blue > 255) {
                bruh.play();
                return;
            }
            new Promise(async (res, rej) => {
                try {
                    let response = await fetch(`http://localhost:3000/rgb-to-hex?red=${red}&green=${green}&blue=${blue}`);
                    let data = await response.json();
                    let hex = data.data;
                    if(hex.length !== 6){
                        bruh.play();
                        return;
                    }
                    document.getElementById('hex-color-container').style.backgroundColor = "#" + hex;
                    document.getElementById('hex-output').innerHTML = "#" + hex;
                    wow.play();
                } catch (error) {
                    bruh.play();
                }
                
            });
            break;
    }
}